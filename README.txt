NOTE: If you use this vector, and for details of bias determination, please cite: Perdomo-Ortiz, A. et al. Determination and correction of persistent biases in quantum annealers. Sci. Rep. 6, 18628.
Direct URL for paper: http://www.nature.com/articles/srep18628

**Intructions for working with bias vectors for DW2X at NASA**

In these instructions we will use the bias vector from 2016-02-25. The 20-19 suffix in the name of the file corresponds to the time the data collection started. For the h bias determination, it usuallly only takes a couple of hours for the process to be completed. 
 
If you are working in python, you can load the file with:
 
import scipy.io
hbiases_p0mean_Tmin = scipy.io.loadmat('biases_p0mean_Tmin_2016-02-25_20-19')['hbiases_p0mean_Tmin']
 
As usual, the vector hbiases_p0mean_Tmin is an array with 1152 entries. Remember all you need to do is to subtract this vector to the h vector you are intending to submit to the DW2X. This is, for the ith qubit, the new value to be programmed is (horiginal[i] - hbiases_p0mean_Tmin[i]), instead of simply horiginal[i]. 
